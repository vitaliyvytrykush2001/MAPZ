﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Windows.Forms;
namespace Interpreter
{
    class BlockStatement : IExpression
    // блок для опрацювання окремо виразів в дужках
    // по суті окремий парсер для опрацювання умов в дужках
    {
        private readonly List<IExpression> blocklist;
        private TextBox txt;
        private string str;
        public BlockStatement(List<IExpression> tmp, TextBox t)
        {
            blocklist = tmp;
            txt = t;
        }

        public object Run(Context context)
        {
            foreach (var expression in blocklist)
            {
                expression.Run(context);
                str+=expression.BuildTree();
            }

            txt.Text = str;
            return null;
        }
        public string BuildTree(int l = 0)
        {
            string FuncName = "BlockStatement";
            return FuncName;
        }
    }
}
