﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BrowserMachineLanguage.Browser;

namespace Interpreter
{
    public static class BrowserExtensions
    {
        private delegate bool? BoolHandler(WebBrowser browser);

        private delegate IList<HtmlElement> ListOfHtmlElementsHandler(WebBrowser browser, string name);

        private delegate HtmlElementCollection HtmlCollectionHandler(WebBrowser browser, string name);

        private delegate HtmlElement HtmlElementHandler(WebBrowser browser, string name);

        private delegate HtmlElement MainHtmlElementHandler(WebBrowser browser, string atr, string value);

        public static void AddWaiter(this WebBrowser browser)
        {
            var barrier = (browser.FindForm() as BrowserForm)?.BrowserLoadBarrier;
            barrier?.AddParticipant();
        }

        public static void RemoveWaiter(this WebBrowser browser)
        {
            var barrier = (browser.FindForm() as BrowserForm)?.BrowserLoadBarrier;
            barrier?.RemoveParticipant();
        }

        public static void WaitForLoad(this WebBrowser browser)
        {
            var barrier = (browser.FindForm() as BrowserForm)?.BrowserLoadBarrier;
            barrier?.SignalAndWait();
        }

        public static bool? IsLoading(this WebBrowser browser)
        {
            if (browser.InvokeRequired)
            {
                return browser.Invoke(new BoolHandler(IsLoading), browser) as bool?;
            }

            return browser.ReadyState != WebBrowserReadyState.Complete;
        }

        public static HtmlElement GetElementConcurrent(this WebBrowser browser, string atr, string value)
        {
            if (browser.InvokeRequired)
            {
                return browser.Invoke(new MainHtmlElementHandler(GetElementConcurrent), browser, atr, value) as HtmlElement;
            }
            else
            {
                switch (atr)
                {
                    case "name": 
                        return browser.Document?.All.GetElementsByName(value).OfType<HtmlElement>()
                            .FirstOrDefault();
                    case "class":
                        var elements = new List<HtmlElement>();

                        if (browser.Document?.All == null) return null;
                        foreach (HtmlElement el in browser.Document.All)
                        {
                            if (!Regex.IsMatch(el.GetAttribute("className"), $@"\b{value}\b")) continue;
                            elements.Add(el);
                        }

                        return elements.OfType<HtmlElement>().FirstOrDefault();
                    case "tag":
                        return browser.Document?.GetElementsByTagName(value).OfType<HtmlElement>()
                            .FirstOrDefault();
                    case "id":
                        return browser.Document?.GetElementById(value);
                    default:
                        throw new ArgumentException("Invalid target attribute entered");
                }
            }
        }
    }
}
