﻿using System;
using System.Threading;
using System.Windows.Forms;
using BrowserMachineLanguage.Browser;

namespace Interpreter
{
    public partial class BrowserForm : Form
    {
        private Barrier _browserLoadBarrier;

        public Barrier BrowserLoadBarrier
        {
            get => _browserLoadBarrier;
            set
            {
                if (_browserLoadBarrier == null)
                {
                    _browserLoadBarrier = value;
                }
            }
        }

        public BrowserWrapper Browser
        {
            get => WebBrowser as BrowserWrapper;
            set
            {
                if (WebBrowser == null)
                {
                    WebBrowser = value;
                    this.Controls.Add(WebBrowser);
                    WebBrowser.Dock = DockStyle.Fill;
                }
            }
        }

        public BrowserForm()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
        }

        public void ApplicationClosing(object sender, EventArgs e)
        {
            this._browserLoadBarrier.Dispose();
            this.Invoke((MethodInvoker)this.Close);
        }

        private void BrowserForm_Shown(object sender, EventArgs e)
        {
            _browserLoadBarrier.SignalAndWait();
        }
    }
}
