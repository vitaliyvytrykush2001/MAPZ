﻿using System;
using System.Windows.Forms;

namespace BrowserMachineLanguage.Browser
{
    public class BrowserWrapper : WebBrowser
    {
        private void InitializeComponent()
        {
            Name = "BrowserWrapper";
        }

        public event AbsolutelyCompleteEventHandler AbsolutelyComplete;

        public delegate void AbsolutelyCompleteEventHandler(object sender, EventArgs e);

        private int _onNavigatingCount;
        private int _onNavigatedCount;
        private int _onDocumentCompleteCount;

        public void ClearCounters()
        {
            _onNavigatingCount = 0;
            _onNavigatedCount = 0;
            _onDocumentCompleteCount = 0;
        }

        public bool Busy
        {
            get
            {
                var bBusy = !(_onNavigatingCount <= _onNavigatedCount);
                if (!bBusy)
                {
                    bBusy = (_onNavigatedCount > _onDocumentCompleteCount);
                }
                else
                {
                    bBusy = _onNavigatedCount != _onDocumentCompleteCount;
                    if (!bBusy) bBusy = !(_onNavigatedCount > 0);
                }

                return bBusy;
            }
        }

        public void WaitUntilComplete()
        {
            while (!Busy)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(1);
            }

            while (Busy)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(1);
            }
        }

        public BrowserWrapper()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigating(WebBrowserNavigatingEventArgs e)
        {
            _onNavigatingCount += 1;
            base.OnNavigating(e);
            if (!Busy)
                OnAbsolutelyComplete();
        }

        protected override void OnNavigated(WebBrowserNavigatedEventArgs e)
        {
            _onNavigatedCount += 1;
            base.OnNavigated(e);
            if (!Busy)
                OnAbsolutelyComplete();
        }

        protected override void OnDocumentCompleted(WebBrowserDocumentCompletedEventArgs e)
        {
            _onDocumentCompleteCount += 1;
            base.OnDocumentCompleted(e);
            if (!Busy)
                OnAbsolutelyComplete();
        }

        public void NavigateAndWait(string url)
        {
            var uri = new Uri(url);
            ClearCounters();
            Navigate(uri);
            WaitUntilComplete();
        }

        public void NavigateAndWait(Uri url)
        {
            ClearCounters();
            Navigate(url);
            WaitUntilComplete();
        }

        protected void OnAbsolutelyComplete()
        {
            ClearCounters();
            AbsolutelyComplete?.Invoke(this, new EventArgs());
        }

    };
}
