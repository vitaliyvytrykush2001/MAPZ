﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrowserMachineLanguage.Browser;

namespace Interpreter
{
    class ClickCommand:IExpression
    {
        private IExpression Attr { get; }
        private IExpression Value { get; }

        public ClickCommand(ICollection<IExpression> arg)
        {
            if (arg.Count!=2)
            {
                throw new Exception(message:"Incorrect count of parameters in click command.");
            }
            Attr = arg.First();
            Value = arg.Skip(1).First();
        }

        public object Run(Context context)
        {
            var arg1 = (string) Attr.Run(context);
            var arg2 = (string) Value.Run(context);


            HtmlElement element = context.SelectedBrowser.Browser.GetElementConcurrent(arg1,arg2);

            element.InvokeMember("click");

            if (context.SelectedBrowser.Browser.Busy)
            {
                context.SelectedBrowser.Browser.WaitUntilComplete();
            }

            return null;
        }
        public string BuildTree(int l = 0)
        {
            string FuncName = "ClickCommand";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName + ":");
            res.Append(Attr.BuildTree(l + 1));
            res.Append(Value.BuildTree(l + 1));
            return res.ToString();
        }
    }
}
