﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class CloseCommand : IExpression
    {
        private IExpression Name { get; }

        public CloseCommand(ICollection<IExpression> arg)
        {
            if (arg.Count != 1)
            {
                throw new Exception(message: "Incorrect count of parameters in click command.");
            }
            this.Name = arg.First();
        }

        public object Run(Context context)
        {
            var arg = (string)this.Name.Run(context);

            context.Close(arg);

            return null;
        }

        public string BuildTree(int l = 0)
        {
            string FuncName = "CloseCommand";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName + ":");
            res.Append(Name.BuildTree(l + 1));
            return res.ToString();
        }
    }
}
