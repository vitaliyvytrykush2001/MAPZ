﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Commands
{
    class GoForwardCommand:IExpression
    { 
            public object Run(Context context)
            {
                context.GoForward();
                return null;
            }

            public string BuildTree(int l = 0)
            {
                string FuncName = "GoForwardCommand";
                var res = new StringBuilder();
                for (int i = 0; i < l; ++i)
                {
                    res.Append("|\t");
                }
                res.AppendLine(FuncName);
                return res.ToString();
            }
        }
    }

