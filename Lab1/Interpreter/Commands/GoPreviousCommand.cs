﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class GoPreviousCommand:IExpression
    {
        public object Run(Context context)
        {
            context.GoPrevious();
            return null;
        }

        public string BuildTree(int l = 0)
        {
            string FuncName = "GoPreviousCommand";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName);
            return res.ToString();
        }
    }
}
