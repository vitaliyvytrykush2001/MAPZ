﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interpreter
{
    class OpenCommand : IExpression
    {
        private IExpression Url { get; }
        private IExpression Name { get; }

        public OpenCommand(ICollection<IExpression> arg)
        {
            if (arg.Count == 2)
            {
                Url = arg.First();
                Name = arg.Skip(1).First();
            }
            else if (arg.Count == 1)
            {
                Url = arg.First();
            }
            else
            {
                throw new Exception(message: "Incorrect count of parameters in open command.");
            }
        }

        public object Run(Context context)
        {

            if (Name != null)
            {
                var arg1 = (string)Url.Run(context);
                var arg2 = (string)Name.Run(context);
                context.Open(arg1, arg2);
            }
            else
            {
                var arg1 = (string)Url.Run(context);
                context.Open(arg1);
            }

            return null;
        }

        public string BuildTree(int l = 0)
        {
            string FuncName = "OpenCommand";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName + ":");
            res.Append(Url.BuildTree(l + 1));
            if (Name!=null)
            {
                res.Append(Name.BuildTree(l + 1));
            }

            return res.ToString();
        }
    }
}
