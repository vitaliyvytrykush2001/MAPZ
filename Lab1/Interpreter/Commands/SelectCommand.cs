﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Commands
{
    class SelectCommand:IExpression
    {
        private IExpression Select { get; }

        public SelectCommand(ICollection<IExpression> arg)
        {
            if (arg.Count != 1)
            {
                throw new Exception(message: "Incorrect count of parameters in select command.");
            }
            Select = arg.First();
        }

        public object Run(Context context)
        {
            var arg = (string) Select.Run(context);
            context.SelectBrowser(arg);
            return null;
        }

        public string BuildTree(int l = 0)
        {
            string FuncName = "SelectCommand";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName + ":");
            res.Append(Select.BuildTree(l + 1));
            return res.ToString();
        }
    }
}
