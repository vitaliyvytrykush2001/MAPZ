﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;

namespace Interpreter
{
    class WriteDownCommand:IExpression
    {
        private IExpression Attr { get; }
        private IExpression Value { get; }
        private IExpression Text { get; }

        public WriteDownCommand (ICollection<IExpression> arg)
        {
            if (arg.Count != 3)
            {
                throw new Exception(message: "Incorrect count of parameters in writeDown command.");
            }
            Attr = arg.First();
            Value = arg.Skip(1).First();
            Text = arg.Skip(2).First();
        }

        public object Run(Context context)
        {

            var arg1 = (string) Attr.Run(context);
            var arg2 = (string) Value.Run(context);
            var arg3 = (string) Text.Run(context);

            var field=context.SelectedBrowser.Browser.GetElementConcurrent(arg1, arg2);
            field.SetAttribute("value",field.GetAttribute("value")+arg3);

            return null;
        }
        public string BuildTree(int l = 0)
        {
            string FuncName = "WriteDownCommand";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName + ":");
            res.Append(Attr.BuildTree(l + 1));
            res.Append(Value.BuildTree(l + 1));
            res.Append(Text.BuildTree(l + 1));
            return res.ToString();
        }
    }
}
