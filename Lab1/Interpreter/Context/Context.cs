﻿using BrowserMachineLanguage.Browser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Interpreter
{
    class Context
    {
        public String TreeString { get; }
        private readonly Dictionary<string, BrowserWrapper> webList;
        private readonly List<Thread> ThreadList;
        public BrowserForm SelectedBrowser;
        private delegate void BringBrowser(string name);
        private event EventHandler ForCloseCommand;


        public Context()
        {
            webList = new Dictionary<string, BrowserWrapper>();
            ThreadList = new List<Thread>();
        }

        public void Open(string url, string name)
        {
            Barrier barrier = new Barrier(2);
            Thread thread = new Thread(() =>
            {
                var webBrowser = new BrowserWrapper();
                webBrowser.NavigateAndWait(new Uri("http://" + url));
                webList.Add(name, webBrowser);
                var browser = new BrowserForm();
                browser.Text = name;
                browser.Browser = webBrowser;
                SelectedBrowser = browser;
                browser.BrowserLoadBarrier = barrier;
                Application.Run(browser);
            });
            thread.SetApartmentState(ApartmentState.STA);
            ThreadList.Add(thread);
            thread.Start();
            barrier.SignalAndWait();
            barrier.Dispose();
            while (SelectedBrowser.Browser.Busy)
            {
                SelectedBrowser.Browser.WaitUntilComplete();
            }

        }

        public void Open(string url)
        {
            Barrier barrier = new Barrier(2);
            Thread thread = new Thread(() =>
            {
                var webBrowser = new BrowserWrapper();
                webBrowser.NavigateAndWait(new Uri("http://" + url));
                webList.Add((webList.Count).ToString(), webBrowser);
                var browser = new BrowserForm();
                browser.Text = (webList.Count - 1).ToString();
                browser.Browser = webBrowser;
                SelectedBrowser = browser;
                browser.BrowserLoadBarrier = barrier;
                Application.Run(browser);
            });
            thread.SetApartmentState(ApartmentState.STA);
            ThreadList.Add(thread);
            thread.Start();
            barrier.SignalAndWait();
            barrier.Dispose();
            while (SelectedBrowser.Browser.Busy)
            {
                SelectedBrowser.Browser.WaitUntilComplete();
            }
        }

        public void Close(string name)
        {
          
                ICollection<string> keys = webList.Keys;
                int i = 0;

                foreach (var tmp in keys)
                {
                    if (tmp == name) break;
                    ++i;
                }
                if (ThreadList[i].IsAlive)
                {
                    ForCloseCommand?.Invoke(this, new EventArgs());
                    ThreadList[i].Abort();
                }
        }

        public void SelectBrowser(string name)
        {
            if (!webList.ContainsKey(name))
            {
                throw new Exception(message: "Selected browser is missing.");
            }
            SelectedBrowser = webList[name].FindForm() as BrowserForm;
            this.BringToFront();
        }


        private void BringToFront()
        { 

            if (SelectedBrowser.InvokeRequired)
            {
                SelectedBrowser.Invoke(new Action(this.BringToFront));
            }
            else
            {
                SelectedBrowser.TopMost = true;
            }

        }

        public void GoPrevious()
        {
                int i = 0;
                foreach (var tmp in webList.Keys)
                {
                    if (tmp != SelectedBrowser.Text)
                    {
                        ++i;
                    }
                    else
                    {
                        break;
                    }
                }
                --i;
                if (i>webList.Keys.Count)
                {
                     throw new Exception(message:"GoPreviousCommand impossible.");
                }
                SelectBrowser(webList.Skip(i).First().Key);
        }

        public void GoForward()
        { 
                var browser = webList.SkipWhile(k => k.Key != SelectedBrowser.Text);
                if (browser==null)
                {
                    throw new Exception(message: "GoForwardCommand impossible.");
                }
                browser = browser.Skip(1);
                SelectBrowser(browser.First().Key);
        }
    }
}