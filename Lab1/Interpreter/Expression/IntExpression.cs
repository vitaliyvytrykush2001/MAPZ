﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class IntExpression : IExpression
    {
        private int Value { get; }

        public IntExpression (int value)
        {
            Value = value;
        }

        public object Run(Context context)
        {
            return Value;
        }

        public string BuildTree(int l = 0)
        {
            string FuncName = "Integer";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName + " : " + Value);
            return res.ToString();
        }
    }
}
