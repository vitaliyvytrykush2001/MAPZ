﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class LoopExpression : IExpression
    {

        private IExpression Counter { get; }
        private IExpression Func { get; }
        public LoopExpression( IExpression arg1, IExpression arg2)
        {
            Counter = arg1;
            Func = arg2;
        }

        public object Run(Context context)
        {
            var arg = (int)Counter.Run(context);

            while(arg!=0)
            {
                Func.Run(context);
                --arg;
            }
            return null;
        }
        public string BuildTree(int l = 0)
        {
            string FuncName = "Loop";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName + " : ");
            res.Append(Counter.BuildTree(l + 1));
            res.Append(Func.BuildTree(l + 1));
            return res.ToString();
        }
    }
}
