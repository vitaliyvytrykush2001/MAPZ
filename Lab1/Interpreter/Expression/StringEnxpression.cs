﻿using System.Text;

namespace Interpreter.Expression
{
    class StringExpression : IExpression
    {
        private string Expr;

        public StringExpression(string expression)
        {
            Expr = expression;
        }

        public object Run(Context context)
        {
            return Expr;
        }

        public string BuildTree(int l = 0)
        {
            string FuncName = "String";
            var res = new StringBuilder();
            for (int i = 0; i < l; ++i)
            {
                res.Append("|\t");
            }
            res.AppendLine(FuncName+" : "+Expr);
            return res.ToString();
        }
        public string TreeBuilder()
        {

            return null;
        }
    }
}
