﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace Interpreter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public void Input (string str)
        {
            TreeOutput.Text = str;
        }
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.runButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.ProgramCode = new FastColoredTextBoxNS.FastColoredTextBox();
            this.TreeOutput = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ProgramCode)).BeginInit();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.runButton.Location = new System.Drawing.Point(13, 317);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(252, 32);
            this.runButton.TabIndex = 0;
            this.runButton.Text = "R U N";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clearButton.Location = new System.Drawing.Point(297, 317);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(252, 32);
            this.clearButton.TabIndex = 1;
            this.clearButton.Text = "C L E A R";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // ProgramCode
            // 
            this.ProgramCode.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.ProgramCode.AutoScrollMinSize = new System.Drawing.Size(29, 16);
            this.ProgramCode.BackBrush = null;
            this.ProgramCode.CharHeight = 16;
            this.ProgramCode.CharWidth = 9;
            this.ProgramCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ProgramCode.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.ProgramCode.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.ProgramCode.IsReplaceMode = false;
            this.ProgramCode.Location = new System.Drawing.Point(13, 12);
            this.ProgramCode.Name = "ProgramCode";
            this.ProgramCode.Paddings = new System.Windows.Forms.Padding(0);
            this.ProgramCode.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.ProgramCode.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("ProgramCode.ServiceColors")));
            this.ProgramCode.Size = new System.Drawing.Size(536, 299);
            this.ProgramCode.TabIndex = 2;
            this.ProgramCode.Zoom = 100;
            // 
            // TreeOutput
            // 
            this.TreeOutput.Location = new System.Drawing.Point(13, 355);
            this.TreeOutput.Multiline = true;
            this.TreeOutput.Name = "TreeOutput";
            this.TreeOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TreeOutput.Size = new System.Drawing.Size(536, 432);
            this.TreeOutput.TabIndex = 3;
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(561, 744);
            this.Controls.Add(this.TreeOutput);
            this.Controls.Add(this.ProgramCode);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.runButton);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ProgramCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void runButton_Click(object sender, EventArgs e)
        {
            try
            {
                Tokenizer tokenizer = new Tokenizer();
                Lexer lexer = new Lexer(tokenizer);
                Queue<Token> tokenList = lexer.Tokenize(ProgramCode.Text);
                Parser parser = new Parser();
                var parse = parser.Parse(tokenList, TreeOutput);
                Context context = new Context();
                parse.Run(context);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            TreeOutput.Clear();
        }
    }
}
