﻿namespace Interpreter
{
    internal interface IExpression
    {
       object Run(Context context);

        string BuildTree(int l = 0);
    }
}
