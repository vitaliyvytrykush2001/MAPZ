﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;


namespace Interpreter
{
    class Lexer
    {
        private readonly Tokenizer tokensDef;

        public Lexer(Tokenizer _token)
        {
            tokensDef = _token;
        }

        public Queue<Token> Tokenize(string programText)
        {
            if (programText == "")
            {
                throw new Exception(message: "Program code field is empty.");
            }
            Queue<Token> tokens = new Queue<Token>();
            var remainingText = "" + programText;

            while (!string.IsNullOrEmpty(remainingText))
            {
                foreach (var exp in tokensDef.TokenDefinition.Keys)
                {
                    var match = MatchCheker(remainingText, exp);
                    if (match == null)
                    {
                        continue;
                    }
                    remainingText = remainingText.Substring(match.Value.Length);
                    if (tokensDef.TokenDefinition[exp] == TokenType.Empty)
                    {
                        break;
                    }

                    tokens.Enqueue(new Token(tokensDef.TokenDefinition[exp], match.Value));
                    {
                        break;
                    }
                }
            }

            return tokens;
        }

        private Match MatchCheker(string txt1, string txt2)
        {
            Match match = Regex.Match(txt1, txt2);

            if (match.Index == 0 && !string.IsNullOrEmpty(match.Value))
            {
                return match;
            }
            else
            {
                return null;
            }
        }
    }

}