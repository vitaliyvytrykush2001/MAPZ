﻿using Interpreter.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Interpreter.Commands;

namespace Interpreter
{

    class Parser
    {
        private Queue<Token> _token;
        private TextBox textBox;
        public IExpression Parse(Queue<Token> token, TextBox t)
        {
            textBox = t;
            _token = token;
            List<IExpression> expressions = new List<IExpression>();
            while (_token.Count != 0)
            {
                expressions.Add(FindStatemants());
            }
            return new BlockStatement(expressions,textBox);
        }

        private IExpression FindStatemants()
        {
            IExpression expression;
            switch (_token.Peek().TokenType)
            {
                case TokenType.OpenParenthesis:
                    return BlockOperation();
                case TokenType.CloseParenthesis:
                    return null;
                case TokenType.FunctionEnd:
                    return null;
                case TokenType.Loop:
                    return GetLoop();
          
                default:
                    expression = BasicFunctions(); 
                    if (_token.Dequeue().TokenType != TokenType.FunctionEnd)
                    {
                        throw new Exception(message: "Incorrect end of operation.");
                    }
                    return expression;
            }
        }


        private IExpression FindExpression()
        {
            IExpression expression;

            switch (_token.Peek().TokenType)
            {
                case TokenType.StringValue:
                    return new StringExpression(_token.Dequeue().Value.Trim('"'));
                case TokenType.IntValue:
                    return new IntExpression(int.Parse(_token.Dequeue().Value));   
            }

            throw new Exception(message: "Not consist current value type. (FindExpression)");
        }

        private IExpression BasicFunctions()
        {
            IExpression expression;
            switch (_token.Peek().TokenType)
            {
                case TokenType.Open:
                    _token.Dequeue();
                    var arg = ReadArg();
                    return new OpenCommand(arg);
                case TokenType.Close:
                    _token.Dequeue();
                    var arg1 = ReadArg();
                    return new CloseCommand(arg1);
                case TokenType.Click:
                    _token.Dequeue();
                    var arg2 = ReadArg();
                    return new ClickCommand(arg2);
                case TokenType.SelectWindow:
                    _token.Dequeue();
                    var arg3 = ReadArg();
                    return new SelectCommand(arg3);
                case TokenType.GoForward:
                    _token.Dequeue();
                    return new GoForwardCommand();
                case TokenType.GoPrevious:
                    _token.Dequeue();
                    return new GoPreviousCommand();
                case TokenType.WriteDown:
                    _token.Dequeue();
                    var arg4 = ReadArg();
                    return new WriteDownCommand(arg4);
                default:
                    return null;
            }
        }

        private LoopExpression GetLoop()
        {

            if (_token.Dequeue().TokenType != TokenType.Loop) 
            {
                throw new Exception(message:"incorrect loop expression. (loop type)");
            }
            
            var count = ReadArg();
            if (count.Count != 1)
            {
                throw new Exception(message:"Incorrect count expression.");
            }

            if(_token.Dequeue().TokenType!=TokenType.Colon)
            {
                throw new Exception(message: "incorrect loop expression. (colon missed)");
            }
            
            var function = FindStatemants();
            return new LoopExpression(count.First(), function);
        }

        private BlockStatement BlockOperation() //опрацювання умов в дужках
        {
            List<IExpression> expression = new List<IExpression>();
            if (_token.Dequeue().TokenType != TokenType.OpenParenthesis)
            {
                throw new Exception(message: "Wrong BlockStatement syntaxis! (OpenParenthesis)");
            }

            var newExpression = FindStatemants();
            if (newExpression == null & _token.Peek().TokenType == TokenType.CloseParenthesis)
            {
                _token.Dequeue();
                return new BlockStatement(expression, textBox);
            }

            expression.Add(newExpression);

            while (_token.Dequeue().TokenType != TokenType.CloseParenthesis)
            {
                IExpression expr = FindStatemants();
                if (expr == null)
                {
                    continue;
                }
                expression.Add(expr);
            }

            
            if (_token.Dequeue().TokenType != TokenType.CloseParenthesis|| _token.Dequeue().TokenType != TokenType.Colon)
            {
                throw new Exception(message: "Wrong BlockStatement syntaxis! (Close Parenthesis)");
            }

            BlockStatement ret = new BlockStatement(expression, textBox);
            return ret;
        }

        private ICollection<IExpression> ReadArg()
        {
            ICollection<IExpression> ColExpress = new List<IExpression>();
            if (_token.Dequeue().TokenType != TokenType.OpenParenthesis)
            {
                throw new Exception(message: "Wrong BlockStatement syntaxis! (OpenParenthesis)");
            }

            while (_token.Peek().TokenType != TokenType.FunctionEnd & _token.Peek().TokenType != TokenType.CloseParenthesis)
            {

                ColExpress.Add(FindExpression());
                if (_token.Peek().TokenType == TokenType.CloseParenthesis)
                {
                    break;
                }

                if (_token.Dequeue().TokenType != TokenType.Comma)
                {
                    throw new Exception(message: "Wrong BlockStatemant syntaxis! (Comma)");
                }
            }

            _token.Dequeue();

            return ColExpress;
        }
    }
}
