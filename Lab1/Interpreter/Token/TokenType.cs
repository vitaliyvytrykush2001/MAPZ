﻿namespace Interpreter
{
    public enum TokenType
    {
        Click,
        Close,
        Comma,
        Colon,
        Open,
        OpenParenthesis,
        CloseParenthesis,
        SelectWindow,
        FunctionEnd,
        StringValue,
        IntValue,
        WriteDown,
        GoForward,
        GoPrevious,
        Loop,
        Empty
    }
}
