﻿using System.Collections.Generic;

namespace Interpreter
{
    class Tokenizer
    {
        public Dictionary<string, TokenType> TokenDefinition { get; }

        public Tokenizer()
        {
            TokenDefinition = new Dictionary<string, TokenType>
            {
                { @"^click",TokenType.Click },
                { @"^close",TokenType.Close },
                { @"^,", TokenType.Comma},
                { @"^:",TokenType.Colon},
                { @"^open",TokenType.Open },
                { @"^\(",TokenType.OpenParenthesis },
                { @"^\)",TokenType.CloseParenthesis },
                { @"^selectWindow",TokenType.SelectWindow },
                { @"^;",TokenType.FunctionEnd },
                { @"^""[^""]*""",TokenType.StringValue },
                { @"^\d+",TokenType.IntValue},
                { @"^goForward", TokenType.GoForward},
                { @"^goPrevious", TokenType.GoPrevious},
                { @"^writeDown",TokenType.WriteDown },
                { @"^loop",TokenType.Loop },
                { @"^\s+",TokenType.Empty }
            };
        }
    }
}
