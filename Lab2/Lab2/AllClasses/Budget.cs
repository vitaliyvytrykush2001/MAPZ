﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab2
{
    class Budget
    {
        private List<Person> AccountantList;

        private static readonly Lazy<Budget> lazy =
            new Lazy<Budget>(() => new Budget());

        public Budget()
        {
            AccountantList=new List<Person>();
        }

        void AddReport(Person obj)
        {
            AccountantList.Add(obj);
        }

        void CreateMonthReport(string path)
        {
            FileStream aFile = new FileStream("MonthReport.txt", FileMode.OpenOrCreate);
        }
        void CreateYearReport(string path)
        {
            FileStream aFile = new FileStream("YearReport.txt", FileMode.OpenOrCreate);
        }

        public static Budget GetInstance()
        {
            return lazy.Value;
        }
    }
}
