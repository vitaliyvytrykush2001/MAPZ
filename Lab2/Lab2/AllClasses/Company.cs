﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Company
    {
        private List<Customer> CustomerList; 
        private static object syncRoot = new Object();
        private Hashtable teams = new Hashtable();
        public Company()
        {
            CustomerList=new List<Customer>();
            Accountant accountant = new Accountant("Maria", "Kukuha", 00001);
        }
        private static Company _instance;
        public static Company GetInstance()
        {
            if (_instance == null)
            {
                lock (syncRoot)
                {
                    _instance = new Company();
                }
            }
            return _instance;
        }
        public void AddCustomer(Customer obj)
        {
            CustomerList.Add(obj);
            Console.WriteLine("Customer was added");
        }
        public Person CreateProjManagerTeam(string Platform, string OS, Composite branch1, Composite branch2)
        {
            ProjManager projManager = new ProjManager("Vasia", "Pupkin", 00002);
            projManager.CreateTeam(Platform, OS,branch1, branch2);
            return projManager;
        }
    }
}
