﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    abstract class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }
        public int ID { get; set; }

        public Person(){}
        public Person(string Name, string Surname, int ID)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Role = Role;
            this.ID = ID;
        }

        public virtual void Add(Person component)
        {
        }

        public virtual void Remove(Person component)
        {
        }

        public abstract string Operation();
    }

    class Composite : Person
    {
        protected List<Person> _children = new List<Person>();

        public override void Add(Person component)
        {
            this._children.Add(component);
        }

        public override void Remove(Person component)
        {
            this._children.Remove(component);
        }

        public override string Operation()
        {
            int i = 0;
            string result = "Branch(";

            foreach (Person component in this._children)
            {
                result += component.Operation();
                if (i != this._children.Count - 1)
                {
                    result += "+";
                }
                i++;
            }

            return result + ")";
        }
    }
}


/*
class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();
            Leaf leaf = new Leaf();
            Console.WriteLine("Client: I get a simple component:");
            client.ClientCode(leaf);
            Composite tree = new Composite();
            Composite branch1 = new Composite();
            branch1.Add(new Leaf());
            branch1.Add(new Leaf());
            Composite branch2 = new Composite();
            branch2.Add(new Leaf());
            tree.Add(branch1);
            tree.Add(branch2);
            Console.WriteLine("Client: Now I've got a composite tree:");
            client.ClientCode(tree);

            Console.Write("Client: I don't need to check the components classes even when managing the tree:\n");
            client.ClientCode2(tree, leaf);
        }
    }
}*/
