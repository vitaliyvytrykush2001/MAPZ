﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Programmer : Person
    {
        public string Rank { get; set; }
        public string Technology { get; set; }
        public Programmer(string Name, string Surname, int ID, string Rank, string Technology) : base(
            Name, Surname, ID)
        {
            this.Rank = Rank;
            this.Technology = Technology;
            Role = "Programer";
            Console.WriteLine("Programmer was created");
        }

        public override string Operation()
        {
            return "Programmer";
        }

    }
}
