﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class TeamLeader : Person
    {
        private string ProjName;
        private string CurrentTask;
        private string ProjGit;
        public TeamLeader(string Name, string Surname,int ID, string ProjName) : base(Name, Surname, ID)
        {
            this.ProjName = ProjName;
            Role = "TeamLeader";
            Console.WriteLine("TeamLeader was created");
        }

        protected List<Person> _children = new List<Person>();

        void SetCurrentTask(string Task)
        {
            this.CurrentTask = Task;
        }

        void CreateRequairements(string path)
        {
            this.ProjGit = path;
        }

        public override string Operation()
        {
            return "TeamLead";
        }
    }
}
