﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace Lab2
{
    class Budget
    {
        private string _state;

        private List<Person> AccountantList;

        private int budget;

        private List<string> operations;

        private static readonly Lazy<Budget> lazy =
            new Lazy<Budget>(() => new Budget());

        public Budget()
        {
            Console.WriteLine("Budget: Constructor. My state: initialized");
            AccountantList = new List<Person>();
            operations=new List<string>();
            budget = 500000;
        }

        void AddReport(Person obj)
        {
            AccountantList.Add(obj);
            Console.WriteLine("Budget: Accountant was added");
        }

        private void MoneyPlus(int sum)
        {
            operations.Add("+ sum");
            budget += sum;
        }

        private void MoneyMinus(int sum)
        {
            operations.Add("- sum");
            budget -= sum;
        }

        private string GenerateRandomString(int length = 10)
        {
            string allowedSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string result = string.Empty;

            while (length > 0)
            {
                result += allowedSymbols[new Random().Next(0, allowedSymbols.Length)];

                Thread.Sleep(12);

                length--;
            }

            return result;
        }

        public static Budget GetInstance()
        {
            return lazy.Value;
        }
        public void DoSomething()
        {
            Console.WriteLine("Originator: I'm doing something important.");
            this._state = this.GenerateRandomString(30);
            Console.WriteLine($"Originator: and my state has changed to: {_state}");
        }

        public IMemento Save()
        {
            return new CreateBudgetCopy(this._state);
        }

    }

    public interface IMemento
    {
        public DateTime getDate();
        public string getState();
        public string getName();
    }
    [Serializable]
    class CreateBudgetCopy : IMemento
    {
        private string _state;
        private DateTime _date;

        public CreateBudgetCopy(string _state)
        {
            this._state = _state;
            this._date=DateTime.Now;
        }

        public DateTime getDate()
        {
            return this._date;
        }

        public string getState()
        {
            return this._state;
        }

        public string getName()
        {
            return $"{this._date} / ({this._state.Substring(0, 9)})...";
        }
    }

    class CopySaver
    {
        private List<IMemento> CopyList = new List<IMemento>();

        private List<BinaryFormatter> BinaryCopyList = new List<BinaryFormatter>();

        private Budget _budget = null;

        public CopySaver(Budget _budget)
        {
            this._budget = _budget;
        }

        public void Backup()
        {
            Console.WriteLine("Budget->CopySaver: Creating backup");
            this.CopyList.Add(this._budget.Save());
            using (FileStream fs = new FileStream("backup_binnary.dat", FileMode.OpenOrCreate))
            {
                BinaryFormatter bformatter = new BinaryFormatter();
                bformatter.Serialize(fs, _budget);
                BinaryCopyList.Add(bformatter);
                Console.WriteLine("Budget->CopySaver: Creating binary backup");
            }
        }

        public void History()
        {
            Console.WriteLine("Budget->CopySaver: This is history:");
            foreach (var tmp in CopyList)
            {
                Console.WriteLine(tmp.getName());
            }

        }
    }
}
