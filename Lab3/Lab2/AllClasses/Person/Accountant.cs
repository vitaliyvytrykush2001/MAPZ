﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Accountant : Person
    {
        private string ProjName;
        private bool Payed { get; set; }
        private int ProjBudjet { get; set; }


        public Accountant(string Name, string Surname, int ID) : base(Name, Surname, ID)
        {
            Role = "Accountant";
            Console.WriteLine("Accauntant: Constructor");
        }

        public void SetAccountantProject(string ProjName)
        {
            this.ProjName = ProjName;
            
        }

        public Accountant DeepCopy(string newProjName)
        {
            Accountant clone = new Accountant(Name, Surname, ID);
            clone.SetAccountantProject(newProjName);
            return clone;
        }

        public override string Operation()
        {
            return "Accountant";
        }

        public int GetDebt()
        {
            return ProjBudjet;
        }

        public bool GetPaidStatus()
        {
            return Payed;
        }

    }
}
