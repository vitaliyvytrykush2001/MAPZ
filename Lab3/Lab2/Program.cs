﻿using System;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Company comp = new Company();
            Customer cust = new Customer("Vovka", "Commander", "FirstProject");
            comp.AddCustomer(cust);
            Composite tree= new Composite();
            Composite branch1 =new Composite();
            Composite branch2 =new Composite();
            cust.ClientCode(comp.CreateProjManagerTeam("Desktop", "IOSD", branch1, branch2));
            tree.Add(branch1);
            tree.Add(branch2);
            cust.ClientCode(tree);
            Console.ReadLine();
        }
    }
}


