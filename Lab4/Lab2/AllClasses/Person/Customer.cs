﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Customer : State
    {

        private List<IObserver> subscrList = new List<IObserver>();
        private string FilePath;
        private string ProjName;
        private string Name;
        private string Surname;

        public void AddSub(IObserver sub)
        {
            this.subscrList.Add(sub);
        }

        public void RemoveSub(IObserver sub)
        {
            this.subscrList.Remove(sub);
        }

        public void Notify()
        {
            foreach(var tmp in subscrList)
            {
                tmp.Update();
            }
        }

        public Customer(string Name, string Surname, string ProjName)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.ProjName = ProjName;
            Console.WriteLine("Customer: Constructor");
        }

        public void AddRequairementFile(string Path)
        {
            this.FilePath = Path;
        }

        public bool CheckControlPoint(string Path)
        {
            return true;
        }
        public void ClientCode(Person leaf)
        {
            Console.WriteLine($"RESULT: {leaf.Operation()}\n");
        }

        public override void Standart()
        {
            Console.WriteLine("Customer: my state is Standart");
        }
        public override void VIP()
        {
            Console.WriteLine("Customer: my state is Vip");
        }
    }

    class Context
    {
        protected Context _context;

        private State _state = null;

        public Context(State _state)
        {
            this._state = _state;
        }

        public void SetStandartState()
        {
            this._state.Standart();
        }

        public void SetVIPState()
        {
            this._state.VIP();
        }
    }

    abstract class State
    {
        public abstract void Standart();

        public abstract void VIP();
    }

}
