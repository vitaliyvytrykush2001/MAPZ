﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class ProjManager:Person, IObserver
    {
        private string ProjName;

        public ProjManager(string Name, string Surname, int ID) : base(Name, Surname,
            ID)
        {
            Role = "ProjManager";
            Console.WriteLine("ProjManager was created");
        }

        public void SetProjManagerProject(string ProjName)
        {
            this.ProjName = ProjName;
        }

        public override string Operation()
        {
            return "ProjManager";
        }


        private IProductDesktop CreateTeamDesktop(string team, List<Person> lst, string TeamType)
        {
            switch (TeamType)
            {
                case "IOSD":
                    var iosd = new IOSDesktop(lst, team);
                    return iosd;
                case "DNETD":
                    var dnetd = new DNetDesktop(lst, team);
                    return dnetd;
            }
            return null;
        }

        private IProductMobile CreateTeamMobile (string team, List<Person> lst, string TeamType)
        {
            switch (TeamType)
            {
                case "IOSM":
                    var iosm = new IOSMobile(lst, team);
                    return iosm;
                case "DNETM":
                    var bnetm = new DNetMobile(lst, team);
                    return bnetm;
            }
            return null;
        }

        public void CreateTeam(string Platform, string OS, Composite branch1, Composite branch2 )//Тут в перспективі є свторення команди вибираючи людей з бази,
        {                                                                                   //зараз просто вручну створю команду.
            List<Person> teamList = new List<Person>();
            Programmer programer1 = new Programmer("Dodik0", "Pobatkovych0", 01001, "Junior", "UniversalWorker");
            Programmer programer2 = new Programmer("Dodik1", "Pobatkovych1", 01002, "Middle", "UniversalWorker");
            Programmer programer3 = new Programmer("Dodik2", "Pobatkovych3", 01003, "Middle", "UniversalWorker");
            Programmer programer4 = new Programmer("Dodik3", "Pobatkovych4", 01004, "Senior", "UniversalWorker");
            branch1.Add(programer1);
            branch1.Add(programer2);
            branch1.Add(programer3);
            branch1.Add(programer4);
            TeamLeader teamLead = new TeamLeader("Prorab", "Syrota", 02001, "FirstProj");
            branch2.Add(teamLead);
         

            if (Platform == "Mobile") 
            {
                var obj1 = CreateTeamMobile("IOS Team", teamList, OS);
            }
            else if (Platform == "Desktop")
            {
                var obj2 = CreateTeamDesktop("Desktop Team",teamList, OS); 
            }
            else
            {
                throw new Exception("Wrong platform type");
            }
        }
        public void Update()
        {
            Console.WriteLine("ProjManager: this is message from customer");
        }
    }
}
