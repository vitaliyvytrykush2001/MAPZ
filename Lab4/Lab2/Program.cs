﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Lab2
{
    class Program
    {
        static int port = 8005; // порт для приема входящих запросов

        static void Main(string[] args)
        {
            Company comp = new Company();
            Customer cust = new Customer("Vovka", "Commander", "FirstProject");
            comp.AddCustomer(cust);
            Composite tree= new Composite();
            Composite branch1 =new Composite();
            Composite branch2 =new Composite();
           
                IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);

                Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    listenSocket.Bind(ipPoint);

                    listenSocket.Listen(10);

                    Console.WriteLine("Server has started and waiting for connection....");

                    while (true)
                    {
                        Socket handler = listenSocket.Accept();
                        StringBuilder builder = new StringBuilder();
                        int bytes = 0; 
                        byte[] data = new byte[256];

                    cust.ClientCode(comp.CreateProjManagerTeam("Desktop", "IOSD", branch1, branch2, cust));



                    string message = "message sent/";
                        data = Encoding.Unicode.GetBytes(message);
                        handler.Send(data);
                        
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            cust.ClientCode(comp.CreateProjManagerTeam("Desktop", "IOSD", branch1, branch2, cust));
            cust.Notify();
            tree.Add(branch1);
            tree.Add(branch2);
            cust.ClientCode(tree);
            Console.ReadLine();

        }
    }
}


