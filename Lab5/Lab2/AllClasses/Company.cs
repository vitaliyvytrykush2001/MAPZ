﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Company 
    {
        private List<Customer> CustomerList;
        private static object syncRoot = new Object();
        private Hashtable teams = new Hashtable();

        public Company()
        {
            Console.WriteLine("Company: Constructor");
            CustomerList = new List<Customer>();
            Accountant accountant = new Accountant("Maria", "Kukuha", 00001);

        }

        private static Company _instance;

        public static Company GetInstance()
        {
            if (_instance == null)
            {
                lock (syncRoot)
                {
                    _instance = new Company();
                }
            }

            return _instance;
        }

        public void AddCustomer(Customer obj)
        {
            CustomerList.Add(obj);
            Console.WriteLine("company: Customer was added");
        }

        public Person CreateProjManagerTeam(string Platform, string OS, Composite branch1, Composite branch2,
            Customer obj, string ret)
        {
            ProjManager projManager = new ProjManager("Vasia", "Pupkin", 00002);
            obj.AddSub(projManager);
            ret = projManager.CreateTeam(Platform, OS, branch1, branch2);

            return projManager;
        }
    }

    public interface ICommand
    {
        void Execute();
    }

    class CreateTeam : ICommand
    {

        public class MethodParam
        {
            public string Platform { get; set; }
            public string OS { get; set; }
            public Composite branch1 { get; set; }
            public Composite branch2 { get; set; }
            public string Retr { get; set; }
            public Customer custObj { get; set; }
        }

        private MethodParam _params;
        private Company _company;

        public CreateTeam(MethodParam @pr)
        {
            _params = @pr;
        }

        public void Execute()
        {
            _company.CreateProjManagerTeam(_params.Platform, _params.OS, _params.branch1, _params.branch2, _params.custObj,
                _params.Retr);
        }
    }
}
