﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public interface ITeam
    {
        IProductDesktop CreateDesktopApp();
        IProductMobile CreateMobileApp();
    }

    class DNetTeam : ITeam
    {

        public IProductDesktop CreateDesktopApp()
        {
            return new DNetDesktop();
        }

        public IProductMobile CreateMobileApp()
        {
            return new DNetMobile();
        }
    }

    class IOSTeam : ITeam
    {
        public IProductDesktop CreateDesktopApp()
        {
            return new IOSDesktop();
        }

        public IProductMobile CreateMobileApp()
        {
            return new IOSMobile();
        }
    }
    public interface IProductMobile
    {
         string Operation();
    }

    public interface IProductDesktop
    {
        string Operation();
    }

    class DNetDesktop : IProductDesktop
    {
        private List<Person> TeamList { get; set; }
        private string TeamID;
        public DNetDesktop(){}
        public DNetDesktop(List<Person> lst, string TeamID)
        {
            TeamList = lst;
            this.TeamID = TeamID;
            Console.WriteLine("DNetDesktop: Team was created");
        }
        public string Operation()
        {
            return "DNetDesktop: Developing";
        }
    }

    class DNetMobile : IProductMobile
    {
        private List<Person> TeamList { get; set; }
        private string TeamID;
        public DNetMobile(){}
        public DNetMobile(List<Person> lst, string TeamID)
        {
            TeamList = lst;
            this.TeamID = TeamID;
            Console.WriteLine("DNetMobile: Team was created");
        }
        public string Operation()
        {
            return "DNetMobile: developing";
        }
    }

    class IOSDesktop : IProductDesktop
    {
        private List<Person> TeamList { get; set; }
        private string TeamID;
        public IOSDesktop(){}
        public IOSDesktop(List<Person> lst, string TeamID)
        {
            TeamList = lst;
            this.TeamID = TeamID;
            Console.WriteLine("IOSDesktop: Team was created");

        }

        public string Operation()
        {
            return "IOSDesktop: Developing";
        }
    }



    class IOSMobile : IProductMobile
    {
        private List<Person> TeamList { get; set; }
        private string TeamID;
        public IOSMobile(){}
        public IOSMobile(List<Person> lst, string teamID)
        {
            TeamList = lst;
            this.TeamID = teamID;
            Console.WriteLine("IOSMobile: Team was created");
        }

        public string Operation()
        {
            return "IOSMobile: Developing";
        }
    }

    

    abstract class DecoratorDesktop : IProductDesktop
    {
        protected IProductDesktop _object;

        public DecoratorDesktop(IProductDesktop obj)
        {
           this._object = obj;
        }

        public void SetIOSDesktop(IProductDesktop obj)
        {
            this._object = obj;
        }

        public abstract string Operation();
    }

    class DesktopDecorator : DecoratorDesktop
    {
        public DesktopDecorator(IProductDesktop obj) : base(obj)
        {
            Console.WriteLine("Decorator desktop: Constructor");
        }

        public override string Operation()
        {
            var tmp= _object.Operation();
            return "Extended desktop developing" + tmp;
        }
    }


    abstract class DecoratorMobile : IProductDesktop
    {
        protected IProductDesktop _object;

        public DecoratorMobile(IProductDesktop obj)
        {
            this._object = obj;
        }

        public void SetIOSDesktop(IProductDesktop obj)
        {
            this._object = obj;
        }

        public abstract string Operation();
    }

    class MobileDecorator : DecoratorMobile
    {
        public MobileDecorator(IProductDesktop obj) : base(obj)
        {
            Console.WriteLine("Decorator mobile: Constructor");
        }

        public override string Operation()
        {
            var tmp = _object.Operation();
            return "Extended mobile developing" + tmp;
        }
    }
}
